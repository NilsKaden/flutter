import 'package:flutter/material.dart';
import 'dart:developer';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Just Do It Later',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key:key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _active = false;

  void _handleTapboxChanged(bool newValue) {
    setState(() {
      _active = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Tapbox(
      active: _active,
      onChanged: _handleTapboxChanged,
    );
  }
}

class Tapbox extends StatelessWidget {
  Tapbox({Key key, this.active: false, @required this.onChanged}) : super(key: key);

  final bool active;
  final ValueChanged<bool> onChanged;

  void _handleTap() {
    onChanged(!active);
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: _handleTap,
        child: Container(
          child: Center(
            child: Text(
              active ? 'Active' : 'Inactive',
              style: TextStyle(fontSize: 32.0, color: Colors.white),
            ),
          ),
          width: 200.0,
          height: 200.0,
          decoration: BoxDecoration(
            color: active ? Colors.lightGreen[700] : Colors.grey[600],
          ),
        ),
      )
    );
  }
}

/*
class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.items}) : super(key: key);

  final String title;
  final List<String> items;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> _todoItems = [];

  void _addTodoItem() { // (NOT) TODO: rename to _addNotToDoItem()
    print('item added');
    setState((){
      int index = _todoItems.length;
      _todoItems.add('Item ' + index.toString());
    });
  }
  @override

  Widget _buildTodoList() {
    return new ListView.builder(
      itemBuilder: (context, index) {
        if (index > _todoItems.length) {
          return _buildTodoItem(_todoItems[index]);
        }
      }
    );
  }

  Widget _buildTodoItem(String todoText) {
    return new ListTile(
      title: new Text(todoText)
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body:
        TextFormField(
          decoration: const InputDecoration(
            icon: Icon(Icons.add),
            hintText: "What is it you'd like to NOT do?",
            labelText: 'NOT ToDo',
          ),
          onSaved: (String value) {
            _addTodoItem();
          },
          validator: (String value) {
            return value.contains('@') ? "Don't use @. That's the spirit!" : null;
          }
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _addTodoItem,
          tooltip: 'Add Not ToDo Task',
          child: new Icon(Icons.add),
        ),
    );
  }
}
*/
